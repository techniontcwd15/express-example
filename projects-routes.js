const express = require('express');
const fs = require('fs');
const router = express.Router();

router.get('/:id', (req, res) => {
  fs.readFile('./db.json','utf8', (err, file) => {
    const db = JSON.parse(file);

    if(db[req.params.id]) {
      res.send(db[req.params.id]);
    } else {
      res.status(404).send('not found');
    }
  });
});

router.post('/', (req, res) => {
  const title = req.query.title;

  fs.readFile('./db.json','utf8', (err, file) => {
    const db = JSON.parse(file);

    db.push({title: title});
    fs.writeFile('./db.json', JSON.stringify(db), 'utf8');

    res.json(db);
  });
});

module.exports = router;
