const express = require('express');
const app = express();
const projectsRoutes = require('./projects-routes')

app.use('/projects', projectsRoutes);

app.listen(8080);
